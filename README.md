# App. #

## **For developers of this service**

### Prerequisites ###

* Install Node.js
* Install nodemon globally
* Use npm to globally install webpack

### Installation ###

* clone this repo
* install node packages:
* npm install


* To build the package 
```
npm run dev
```

* Run start the server:
```
nodemon server.js
```

### running unit test cases 
* mocha src/test/test 
